const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
	output: {
		path: path.join(__dirname, '/dist'),
		filename: 'index.bundle.js',
	},
	devServer: {
		port: 3000,
		hot: true,
		open: true,
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.(s(a|c)ss)$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			},
			{
				test: /\.(svg|jpg|png|jpe?g|gif)$/i,
				loader: 'file-loader',
				options: {
					outputPath: 'images',
				},
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({ template: './public/index.html' }),
		new MiniCssExtractPlugin(),
	],
}
