import React, { lazy, Suspense } from 'react'
import image1 from './Assets/image1.jpg'
import './app.scss'

const Button = lazy(() => import('./Components/Button/Button'))

function App() {
	return (
		<div>
			<h1>Hii there</h1>
			<h2>this app is created using custom webpack configuration</h2>
			<Suspense fallback={<h1>Wait, i{"'"}m Loading</h1>}>
				<Button>i{"'"}m lazy button </Button>
			</Suspense>
			<br />
			<img src={image1} alt='' height='200px' width='300px' />
		</div>
	)
}

export default App
