import React from 'react'
import PropTypes from 'prop-types'
import './button.scss'

function Button({ children }) {
	return <button className='btn'>{children}</button>
}

Button.propTypes = {
	children: PropTypes.node || PropTypes.string,
}
export default Button
